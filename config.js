export default {
  srcDir: '.',
  distDir: 'dist',
  baseDir: 'brand/kit/kitkatyell/',
  gzip: false,
  webp: false
}
