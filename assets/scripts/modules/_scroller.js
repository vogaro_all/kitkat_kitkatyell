import MoveTo from 'moveto'
import _ from 'lodash'

const moveTo = new MoveTo()

const defaults = {
  tolerance: 0,
  duration: 800,
  easing: 'easeInOutQuart'
}

// See Easing Equations (http://gizma.com/easing/) for more ease function.
const defaultEaseFunctions = {
  easeInOutQuart: (t, b, c, d) => {
    t /= d / 2
    if (t < 1) return (c / 2) * t * t * t * t + b
    t -= 2
    return (-c / 2) * (t * t * t * t - 2) + b
  }
}

export default class {
  constructor(selector, options = {}, easeFunctions = {}) {
    this.selector = selector
    this.options = _.defaults(options, defaults)
    this.easeFunctions = _.defaults(easeFunctions, defaultEaseFunctions)
  }

  init() {
    this.els = document.querySelectorAll(this.selector)

    _.forEach(Object.entries(this.easeFunctions), entry => {
      moveTo.addEaseFunction(entry[0], entry[1])
    })

    this.handleEvents()
  }

  handleEvents() {
    this.handlers = {}

    this.handlers.click = this.onClick.bind(this)
    _.forEach(this.els, el => {
      el.addEventListener('click', this.handlers.click)
    })
  }

  onClick(e) {
    const triggerEl = e.currentTarget
    const href = triggerEl.getAttribute('href') || triggerEl.dataset.scroll

    const targetEl = href ? document.getElementById(href) : document.body

    moveTo.move(targetEl, {
      ...this.options
    })
  }
}
