import 'objectFitPolyfill'
import 'what-input'
import 'intersection-observer'
// import picturefill from 'picturefill'
import Scroller from '../modules/_scroller'
// import MicroModal from 'micromodal'

{
  const scroller = new Scroller('[data-scroll]')
  scroller.init()
}

// 375px以下でviewportを固定
{
  const minWidth = 375
  const viewportEl = document.querySelector('meta[name="viewport"]')
  const mediaQueryList = window.matchMedia(`(min-device-width: 767.5px)`)

  function onChange() {
    const viewportContent = mediaQueryList.matches
      ? 'width=device-width, initial-scale=1'
      : `width=${minWidth}`

    viewportEl.setAttribute('content', viewportContent)
  }

  mediaQueryList.addListener(onChange)
  onChange()
}
